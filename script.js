/**
 * ------ ПРИМЕРЫ 7ого ЗАНЯТИЯ 2ого МОДУЛЯ ---- *
 * -------------------------------------------- *
 */

/*
 * ============================================ *
 * -------- БАЗОВЫЕ ИНТЕРАКТИВНЫЕ ОКНА -------- *
 *                     ALERT                    *
 * -------------------------------------------- *
 */
function demo_alert(){
    alert('Сработало базовое интерактивное окно! \n Нажмите "Ок" чтобы закрыть.');
}

/**
 * -------- БАЗОВЫЕ ИНТЕРАКТИВНЫЕ ОКНА -------- *
 *                    PROMPT                    *
 * -------------------------------------------- *
 */
function demo_prompt(){
    let par_text = document.getElementById('prompt_paragr');
    let prompt_input = prompt('Введите текст', 'текст по умолчанию');
    if (prompt_input) par_text.innerHTML = prompt_input;
}

/**
 * -------- БАЗОВЫЕ ИНТЕРАКТИВНЫЕ ОКНА -------- *
 *                   CONFIRM                    *
 * -------------------------------------------- *
 */
function demo_confirm(){
    let conf_text = document.getElementById('confirm_paragr')
    let conf_res = confirm('Подтвердить?');
    (conf_res) ? conf_text.innerHTML = 'Подтверждено' : conf_text.innerHTML = 'Отменено';
}

/**
 * ============================================ *
 * -------------- ПРИМЕР АНИМАЦИИ ------------- *
 *                 атрибут style                *
 * -------------------------------------------- *
 */
// это 'флаги' чтобы функция
// понимала куда перемещать круг
let moveLeft = false;
let moveUp = false;

function runAnimation() {
    // вызываем нашу функцию каждые 20 миллисекунд
    setInterval(move, 20);
}

// получаем координаты элемента в контексте документа
function getCoords(elem) {
    let box = elem.getBoundingClientRect();

    return {
        top: box.top + window.pageYOffset,
        left: box.left + window.pageXOffset
    };
}

// будет перемещать каждые 20
// миллисекунд наш круг
function move() {

    // получаем доступ к кругу
    let circle = document.getElementById('circle')
    let container = document.getElementById('container');

    // вычисляем реальные координаты
    let cycleX = getCoords(circle).left;
    let cycleY = getCoords(circle).top;
    let containerX = getCoords(container).left;
    let containery = getCoords(container).top;

    // будем использовать переменные чтобы
    // записывать текущее положение круга
    let posX = cycleX - containerX;
    let posY = cycleY - containery;

    /* чтобы определить направление движения круга,
     проверяем достигли ли мы границы, сначала по горизонтали
    */
    if (posX >= 550) moveLeft = true; // значит дошли до правой границы
    if (posX <= 0) moveLeft = false; // значит дошли до левой границы
    if (posY >= 250) moveUp = true; // достигли нижней границы
    if (posY <= 0) moveUp = false; // достигли верхней границы

    // меняем позицию в зависимости
    // от направления движения
    moveLeft ?
        // по горизонтали
        posX = cycleX - 3 : posX = cycleX + 3;
    moveUp ?
        // по вертикали
        posY = cycleY - 1 : posY = cycleY + 1;
    // так мы двигаем наш круг
    circle.style.left = posX+'px';
    circle.style.top = posY+'px';
}
// когда загружаем страницу - запускаем анимацию
window.onload = () => {
    runAnimation();
}

/**
 * ============================================ *
 * -------- ПРИМЕР СЛАЙДЕР ИЗОБРАЖЕНИЙ -------- *
 *                button onclick                *
 * -------------------------------------------- *
 */
// массив с адресами картинок
let images = [
    'assets/1.png','assets/2.png','assets/3.png','assets/4.png','assets/5.png',
]
let num = 0;
// нажимаем кнопу "Вперед"
function forwImg() {
    let slider = document.getElementById('slider');
    num++;
    // если предыдущий был последнем в массиве
    if (num >= images.length) {
        num = 0;
    }
    slider.src = images[num];
}
// нажимаем кнопу "Назад"
function backImg() {
    let slider = document.getElementById('slider');
    num--;
    // если предыдущий был первым в массиве
    if (num < 0) {
        num = images.length - 1;
    }
    slider.src = images[num];
}